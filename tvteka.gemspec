# -*- encoding: utf-8 -*-
VERSION = "0.0.3"

Gem::Specification.new do |spec|
  spec.name          = "tvteka"
  spec.version       = VERSION
  spec.authors       = ["Aleksandr Lossenko"]
  spec.email         = ["aleksandr@byteflip.de"]
  spec.description   = %q{Rubymotion API wrapper for tvteka.com}
  spec.summary       = %q{Rubymotion API wrapper for tvteka.com}
  spec.homepage      = "https://bitbucket.org/egze/tvteka/"
  spec.license       = "MIT"

  files = []
  files << 'README.md'
  files.concat(Dir.glob('lib/**/*.rb'))
  spec.files         = files
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rake"
  spec.add_dependency "dish"
end
