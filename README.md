# tvteka

A native [RubyMotion](http://www.rubymotion.com/) wrapper around the [Tvteka](http://tvteka.com/site/api) API.

##Installation

Gemfile
```ruby
gem 'tvteka'
```

Rakefile
``` ruby
require 'tvteka'
```

then run bundle install and you should be good to go.

Tvteka relies on [STHTTPRequest](https://github.com/nst/STHTTPRequest) to run so you'll need to install the neccessary pods.

```
### First get Cocoapods up and running.
$ pod setup

### Now install the needed pods.
$ rake pod:install
```

## Usage

### Obtain device token

```ruby
Tvteka::Client.register(your_username, your_password) do |response|
  if response.success?
  	response.data # returns instance of Tvteka::Session with token
  else
  	response.error
  end
end
```

### Verify that device token is still valid

```ruby
Tvteka::Client.verify(token) do |response|
  if response.success?
  	response.data # returns instance of Tvteka::Session with authorized attribute
  else
  	response.error
  end
end
```

### Get list of live channels

```ruby
Tvteka::Client.live(token) do |response|
  if response.success?
  	response.data # returns array of Tvteka::Channel
  else
  	response.error
  end
end
```

### Get list of shows for single channel

```ruby
Tvteka::Client.live_channel(token) do |response|
  if response.success?
  	response.data # returns array of Tvteka::Show
  else
  	response.error
  end
end
```

