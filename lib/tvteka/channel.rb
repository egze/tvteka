module Tvteka
  class Channel < Dish::Plate

    #attr_accessor :id, :thumb_url, :videostatus, :favourited, :link, :telecast, :width, :button, :logo, :tz_offset, :schedule_date, :streaming_urls, :timezone, :height, :name, :schedule_link

    def self.live(token, &block)
      Tvteka::API.new.get("http://tvteka.com/live", {cookies: {"deviceToken" => token}}) do |response|
        $bla = response.data
        if response.success?
          block.call Dish(response.data, Tvteka::Channel)
        else
          block.call Tvteka::Error.new(response.data)
        end
      end
    end

    def self.timeshift(channel, token, &block)
      Tvteka::API.new.get("http://tvteka.com/live/#{channel}", {cookies: {"deviceToken" => token}}) do |response|
        $bla = response.data
        if response.success?
          block.call Dish(response.data, Tvteka::Show)
        else
          block.call Tvteka::Error.new(response.data)
        end
      end
    end

    def self.schedule(channel, date, token, &block)
      Tvteka::API.new.get("http://tvteka.com/schedule/#{channel}/#{date.strftime("%Y/%m/%d")}", {cookies: {"deviceToken" => token}}) do |response|
        $bla = response.data
        if response.success?
          block.call Dish(response.data["airtimes"], Tvteka::Show)
        else
          block.call Tvteka::Error.new(response.data)
        end
      end
    end

  end
end
