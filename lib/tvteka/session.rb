module Tvteka

  class Session < Dish::Plate

    def self.register(username, password, params = {}, &block)

      params = { "session[login]" => username, "session[password]" => password, "device[type]" => "XBMC", "device[id]" => "c0:3d:45:28:fc:ad" }.merge(params)

      Tvteka::API.new.get("http://tvteka.com/session/register", params) do |response|
        if response.success? && !has_errors?(response.data)
          block.call Tvteka::Session.new(response.data)
        else
          block.call Tvteka::Error.new(response.data)
        end
      end
    end

    def self.verify(token, &block)
      Tvteka::API.new.get("http://tvteka.com/session/verify", {cookies: {"deviceToken" => token}}) do |response|
        if response.success?
          block.call Tvteka::Session.new(response.data)
        else
          block.call Tvteka::Error.new(response.data)
        end
      end
    end


    def authorized?
      authorized
    end

    def self.has_errors?(data)
      data.is_a?(Array) && (data.map(&:first) & ["login", "user", "venture", "password"]).any?
    end

  end

end
