module Tvteka
  class API

    def get(url, params, &block)
      cookies = params.delete(:cookies)
      params = params.to_a.map {|p| "#{p[0]}=#{p[1]}" }.join("&")
      request = create_request(url + "?" + params, :get)
      add_cookies(request, cookies) if cookies
      create_task(request, &block).resume
    end

    private

    def config
      NSURLSessionConfiguration.defaultSessionConfiguration
    end

    def session
      NSURLSession.sessionWithConfiguration(config)
    end

    def create_request(url, method)
      url = NSURL.URLWithString(url)
      request = NSMutableURLRequest.requestWithURL(url)
      request.addValue("application/vnd.tvp.xbmc+json", forHTTPHeaderField: "Content-Type")
      request.addValue("application/vnd.tvp.xbmc+json", forHTTPHeaderField: "Accept")
      request.setHTTPMethod(method.to_s.upcase)
      request
    end

    def add_cookies(request, cookies)
      cookies.each do |k, v|
        request.setValue("#{k}=#{v}", forHTTPHeaderField: "Cookie")
      end
    end

    def create_task(request, &block)
      if block_given?
        session.dataTaskWithRequest(request, completionHandler: -> (data, response, error) {
          block.call(APIResponse.new(data, response, error))
        })
      else
        session.dataTaskWithRequest(request)
      end
    end

    class APIResponse
      attr_reader :success, :data, :error

      def initialize(data, response, error)
        @success = (200...300).include?(response.statusCode)
        @data = NSJSONSerialization.JSONObjectWithData(data, options:0, error: nil)
        @error = error
      end

      def success?
        @success
      end
    end
  end
end
