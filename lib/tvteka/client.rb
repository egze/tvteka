module Tvteka
  class Client
    def self.session(username, password, options = {}, &block)
      options = { "session[login]" => username, "session[password]" => password, "device[type]" => "xmbc", "device[id]" => "c0:3d:45:28:fc:ad" }.merge(options)
      
      Tvteka::API.new.get(:session, "http://tvteka.com/session/register", options) do |response|
        block.call response
      end
    end
    
    def self.verify(token, &block)
      Tvteka::API.new.get(:verify, "http://tvteka.com/session/verify", {cookies: {"deviceToken" => token}}) do |response|
        block.call response
      end
    end
    
    def self.live(token, &block)
      Tvteka::API.new.get(:live, "http://tvteka.com/live", {cookies: {"deviceToken" => token}}) do |response|
        block.call response
      end
    end
    
    def self.live_channel(token, channel, &block)
      Tvteka::API.new.get(:live_channel, "http://tvteka.com/live/#{channel}", {cookies: {"deviceToken" => token}}) do |response|
        block.call response
      end
    end

    def self.request
      Tvteka::Request.new
    end
  end
end